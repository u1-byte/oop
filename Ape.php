<?php

class Ape extends Animal{
    public $legs = 4;

    public function __construct($name){
        $this->name = $name;
    }

    public function yell(){
        echo "Auooo <br>";
    }
}

?>