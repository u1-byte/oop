<?php
require "animal.php";
require "Ape.php";
require "Frog.php";

$sheep = new Animal("shaun");

echo $sheep->name . "<br>"; // "shaun"
echo $sheep->legs . "<br>"; // 2
echo $sheep->cold_blooded . "<br>";// false
echo "<br>";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

// index.php
$sungokong = new Ape("kera sakti");
echo $sungokong->name . "<br>"; // "kera sakti"
echo $sungokong->legs . "<br>"; // 4
echo $sungokong->cold_blooded . "<br>"; // false
$sungokong->yell(); // "Auooo"
echo "<br>";

$kodok = new Frog("buduk");
echo $kodok->name . "<br>"; // "buduk"
echo $kodok->legs . "<br>"; // 4
echo $kodok->cold_blooded . "<br>"; // true
$kodok->jump() ; // "hop hop"
echo "<br>";

?>